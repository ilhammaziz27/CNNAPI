import numpy as np

class Layer:

    def __init__(self,depth,height,width):
        self.depth = depth
        self.height = height
        self.width = width
        self.data = np.zeros((depth,height,width))


    def displayData(self):
        print(self.data)

class Filter(Layer):
    def __init__(self,depth,height,width):
        self.depth = depth
        self.height = height
        self.width = width
        self.data = np.zeros((depth, height, width))

class App:

    #def __init__(self):

    def Convolve(self,input,filter,stride,padX,padY,bias):
        #Masih salah
        width = int((input.width - filter.width + 2*padX)/stride + 1)
        height = int((input.height - filter.height + 2*padY)/stride + 1)
        depth = 1

        out = Layer(depth,height,width)
        #for l in range(depth):
        input_ = np.pad(input.data[0], [(padX, padY), (padX, padY)], 'constant')
        #print(input_)

        for m in range(height):
            for n in range(width):
                out.data[:,m,n]=np.sum(input_.data[:,m * stride: m * stride + filter.height, n * stride:n * stride + filter.width]*filter.data)+bias
        return out

    def pooling(self,input,filter,stride):
        stride = stride if stride > 0 else 1
        height = int((input.height-filter.height)/stride) + 1
        width = int((input.width - filter.width)/stride) + 1
        depth = input.depth

        output=Layer(depth, height, width)

        for i in range(depth):
            for j in range(height):
                for k in range(width):
                    local=input.data[i, j * stride: j * stride + filter.width, k * stride: k * stride + filter.height]
                    output.data[i,j,k] = np.amax(local)

        return output



x = Layer(3,5,5)
x.data = np.array([
    [[0,0,2,0,2],
     [2,2,1,2,1],
     [2,1,1,2,0],
     [2,2,0,1,2],
     [2,0,1,0,2]],

    [[1,2,0,2,1],
     [0,1,2,0,1],
     [0,0,0,0,1],
     [1,0,0,0,2],
     [0,1,2,0,1]],

    [[0,2,2,1,2],
     [1,2,2,1,1],
     [1,0,0,2,1],
     [2,0,1,0,0],
     [1,0,0,0,2]]
])

#x.displayData()
#w = Layer(3,3,3)
w1 = Filter(3,3,3)
w2 = Filter(3,3,3)
w1.data = np.array([
    [[0,1,0],
     [1,0,1],
     [0,0,0]],

    [[0,0,1],
     [0,-1,1],
     [-1,1,-1]],

    [[0,-1,1],
     [0,1,-1],
     [1,1,1]]])

w1.data = np.array([
    [[0,0,1],
      [-1,1,1],
      [-1,0,-1]],

     [[1,-1,1],
      [1,0,1],
      [-1,1,1]],

     [[0,0,1],
      [0,0,1],
      [1,1,1]]
    ])

cnn = App()
convLayer=cnn.Convolve(x,w1,2,1,1,1).displayData()
#maxPool=cnn.pooling(convLayer,w1,1)
#max.displayData()

#5A
# def convForward(input,w,b,stride,pad):
#     newX=int((input.shape[0]-w.shape[0]+(2*pad))/stride+1)
#     newY=int((input.shape[1]-w.shape[1]+(2*pad))/stride+1)
#     newZ=int(w.shape[2])
#
#     out = np.zeros((newX,newY,newZ))
#     newInput = np.pad(input, ((pad,pad),(pad,pad)), 'constant')
#
#     for m in range(newX):
#         for n in range(newY):
#             for o in range(newZ):
#                 out[m,n,o]=np.sum(newInput[m*stride:m*stride+w.shape[0]:o,n*stride:n*stride+w.shape[1]:o]*w)+b
#     cache = (input,w,b,stride,pad)
#     return out,cache
#
# #5C
# def maxPoolForward(input,h,w,fSize,stride):
#     stride=stride if stride>0 else 1
#
#     newX = int(input.shape[0] - fSize)/stride + 1
#     newY = int(input.shape[1] - fSize)/stride + 1
#     out = np.zeros((newX,newY))
#
#     for i in range(h):
#         for j in range(w):
#             out[i,j]=np.matrix.max(input[i*stride:i*stride+fSize-1,j*stride:j*stride+fSize-1])
#
#     cache = (input, h, w, stride)
#     return out,cache
#
# #5D
# def maxPoolBackward(dout,x,h,w,fSize,stride):
#     stride = stride if stride > 0 else 1
#
#     newX = int(dout.shape[0] - 1)*stride+fSize
#     newY = int(dout.shape[1] - 1)*stride+fSize
#     out = np.zeros((newX, newY))
#
#     for i in range(h):
#         for j in range(w):
#             out[i*stride:i*stride+fSize-1, j*stride:j*stride+fSize-1] = dout[i,j]
#
#     return out
#
# #5B
# def convBackward(dout,x,w,b,stride,pad):
#     newX = int((dout.shape[0] - 1)*stride + w.shape[0] - (2 * pad))
#     newY = int((dout.shape[1] - 1)*stride + w.shape[1] - (2 * pad))
#     newZ = int(w.shape[2])
#
#     out = np.zeros((newX, newY, newZ))
#     newInput = np.pad(input, ((pad, pad), (pad, pad)), 'constant')
#
#     for m in range(newX):
#         for n in range(newY):
#             for o in range(newZ):
#                 out[m, n, o] = np.sum(newInput[m * stride:m * stride + w.shape[0]:o, n * stride:n * stride + w.shape[1]:o] * w) + b
#
#     return out